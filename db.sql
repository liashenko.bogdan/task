-- Adminer 4.8.1 MySQL 5.7.22 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `payment_method`;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `payment_system_id` int(11) NOT NULL,
  `payment_method_type_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `commission` float NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_countries` json DEFAULT NULL,
  `restricted_countries` json DEFAULT NULL,
  `allowed_os` json DEFAULT NULL,
  `restricted_products` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_system_id` (`payment_system_id`),
  KEY `payment_method_type_id` (`payment_method_type_id`),
  CONSTRAINT `payment_method_ibfk_1` FOREIGN KEY (`payment_system_id`) REFERENCES `payment_system` (`id`),
  CONSTRAINT `payment_method_ibfk_2` FOREIGN KEY (`payment_method_type_id`) REFERENCES `payment_method_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `payment_method` (`id`, `image`, `url`, `lang`, `payment_system_id`, `payment_method_type_id`, `name`, `commission`, `is_active`, `allowed_countries`, `restricted_countries`, `allowed_os`, `restricted_products`) VALUES
(1,	'intercassa_card.jpg',	'pay/1',	'en',	1,	1,	'Банковские карты',	2.5,	1,	'null',	'null',	NULL,	NULL),
(2,	'liqpay.jpg',	'pay/2',	'en',	1,	1,	'LiqPay',	2,	1,	'null',	'[\"RU\"]',	NULL,	NULL),
(3,	'wayforpay_terminal.jpg',	'pay/3',	'en',	1,	2,	'Терминалы IBOX',	4,	1,	'[\"IN\"]',	'null',	NULL,	NULL),
(4,	'google_pay.jpg',	'pay/4',	'en',	1,	1,	'GooglePay',	0,	1,	NULL,	'[\"IN\"]',	'[\"android\"]',	NULL),
(5,	'ApplePay.jpg',	'pay/5',	'en',	1,	1,	'ApplePay',	0,	1,	NULL,	NULL,	'[\"ios\"]',	NULL),
(6,	'privatbank1.jpg',	'pay/6',	'en',	4,	1,	'Оплата картой',	3,	1,	NULL,	NULL,	NULL,	NULL),
(7,	'privatbank2.jpg',	'pay/6',	'en',	4,	1,	'Оплата картой ПриватБанка',	3,	1,	NULL,	NULL,	NULL,	NULL),
(9,	'wallet.jpg',	'pay/0',	'en',	5,	5,	'Внутренний кошелек',	0,	1,	NULL,	NULL,	NULL,	'[\"walletRefill\"]'),
(10,	'paypal.jpg',	'pay/10',	'en',	6,	6,	'PayPal',	5,	1,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `payment_method_min_restrictions`;
CREATE TABLE `payment_method_min_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_id` int(11) NOT NULL,
  `min_amount` float NOT NULL DEFAULT '0.3',
  `country` json DEFAULT NULL,
  `product` json DEFAULT NULL,
  `allow` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `payment_method_id` (`payment_method_id`),
  CONSTRAINT `payment_method_min_restrictions_ibfk_1` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `payment_method_min_restrictions` (`id`, `payment_method_id`, `min_amount`, `country`, `product`, `allow`) VALUES
(1,	10,	0.3,	'[\"ES\"]',	NULL,	0),
(3,	9,	0.3,	'[\"ES\"]',	'[\"reward\"]',	1);

DROP TABLE IF EXISTS `payment_method_type`;
CREATE TABLE `payment_method_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `payment_method_type` (`id`, `name`, `position`) VALUES
(1,	'Банковская карта',	1),
(2,	'Терминал',	100),
(3,	'Qiwi Кошелек',	2),
(4,	'Webmoney',	3),
(5,	'Внутренний кошелек',	4),
(6,	'PayPal',	6);

DROP TABLE IF EXISTS `payment_system`;
CREATE TABLE `payment_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `payment_system` (`id`, `name`, `description`, `is_active`) VALUES
(1,	'Interkassa',	'Interkassa',	1),
(2,	'PayU',	'PayU',	1),
(3,	'CardPay',	'CardPay',	1),
(4,	'ПриватБанк',	'ПриватБанк',	1),
(5,	'Внутренний кошелек',	'Внутренний кошелек',	1),
(6,	'PayPal',	'PayPal',	1);

-- 2022-08-30 21:38:02
