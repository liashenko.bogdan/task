<?php

class PaymentTypeSelector
{
    private $productType;
    private $amount;
    private $lang;
    private $countryCode;
    private $userOs;

    public function __construct($productType, $amount, $lang, $countryCode, $userOs)
    {
        //Плохо, что такие параметры передаются в конструкторе и мы не сможем использовать Dependency injection
        //Я бы передавал их методу getButtons

        if (!in_array($productType, ['book', 'reward', 'walletRefill'])) {
            throw new \Error('Incorrect ProductType parameter');
        }

        if (!in_array($lang, ['en', 'es', 'uk'])) {
            throw new \Error('Incorrect Lang parameter');
        }

        if (!in_array($userOs, ['windows', 'android', 'ios'])) {
            throw new \Error('Incorrect UserOs parameter');
        }

        if (empty($countryCode)) {
            throw new \Error('Incorrect CountryCode parameter');
        }

        if (!is_numeric($amount) || $amount < 0) {
            throw new \Error('Incorrect Amount parameter');
        }

        $this->productType = $productType;
        $this->amount      = $amount;
        $this->lang        = $lang;
        $this->countryCode = $countryCode;
        $this->userOs      = $userOs;

    }

    public function getButtons()
    {
        $buttons = [];

        $sqllink = mysqli_connect('localhost', 'root', 'your_mysql_root_password');
        mysqli_select_db($sqllink, 'db');
        mysqli_query($sqllink, 'SET NAMES utf8mb4');
        //Так конечно никто уже неделает, но это быстрее, чем подключать и настраивать ORM
        //Метод прямого запроса менее удобен, чем создание отдельного класса для сущностей метода оплаты и платежной системы
        //Но благодаря одному запросу можно добиться быстрого результата
        //И такой результат легко закешировать по ключу из входящих параметров

        $sql = "SELECT 
            `payment_method`.`id`,
            `payment_method`.`name`,
            `payment_method`.`image`,
            `payment_method`.`commission`,
            `payment_method`.`url`
        FROM 
            `payment_system`	
        LEFT JOIN 
            `payment_method` 
        ON 
            `payment_method`.`payment_system_id` = `payment_system`.`id`
        AND
            `payment_method`.`is_active` = 1
        LEFT JOIN 
            `payment_method_type`
        ON 
            `payment_method_type`.`id` = `payment_method`.`payment_method_type_id`
        LEFT JOIN 
            `payment_method_min_restrictions`
        ON 
            `payment_method_min_restrictions`.`payment_method_id` = `payment_method`.`id`
        AND 
            `payment_method_min_restrictions`.`allow` = 0
        AND 
            `payment_method_min_restrictions`.`min_amount` >= '{$this->amount}'
        AND (
            `payment_method_min_restrictions`.`country` IS NULL
                OR
            JSON_SEARCH(`payment_method_min_restrictions`.`country`,'one', '{$this->countryCode}') IS NOT NULL 
        )
        AND (
            `payment_method_min_restrictions`.`product` IS NULL
                OR
            JSON_SEARCH(`payment_method_min_restrictions`.`product`,'one', '{$this->productType}') IS NOT NULL 
        )
        LEFT JOIN 
            `payment_method_min_restrictions` AS `payment_method_min_restrictions_allow` 
        ON 
           
            `payment_method_min_restrictions_allow`.`allow` = 1
        AND 
            `payment_method_min_restrictions_allow`.`min_amount` >= '{$this->amount}'
        AND (
            `payment_method_min_restrictions_allow`.`country` IS NULL
                OR
            JSON_SEARCH(`payment_method_min_restrictions_allow`.`country`,'one', '{$this->countryCode}') IS NOT NULL 
        )
        AND (
            `payment_method_min_restrictions_allow`.`product` IS NULL
                OR
            JSON_SEARCH(`payment_method_min_restrictions_allow`.`product`,'one', '{$this->productType}') IS NOT NULL 
        )
        WHERE
            `payment_method`.`id` IS NOT NULL
        AND 
            `payment_method`.`lang` = '{$this->lang}'
        AND   
            (
                (
                 `payment_method_min_restrictions`.`id` IS NULL 
                     AND 
                 `payment_method_min_restrictions_allow`.`id` IS NULL 
                )
                OR
                (
                    `payment_method_min_restrictions_allow`.`id` IS NOT NULL 
                        AND 
                     `payment_method_min_restrictions_allow`.`payment_method_id` = `payment_method`.`id`
                )    
            )
        AND 
            `payment_system`.`is_active` = 1
        AND (
            `payment_method`.`allowed_countries` IS NULL
                OR
            JSON_SEARCH(`payment_method`.`allowed_countries`,'one', '{$this->countryCode}') IS NOT NULL 
        )
        AND (
            `payment_method`.`allowed_countries` IS NULL
                OR
            JSON_SEARCH(`payment_method`.`allowed_countries`,'one', '{$this->countryCode}') IS NOT NULL 
        )
        AND (
            `payment_method`.`restricted_countries` IS NULL
                OR
            JSON_SEARCH(`payment_method`.`restricted_countries`,'one', '{$this->countryCode}') IS NULL 
        )
        AND (
            `payment_method`.`allowed_os` IS NULL
                OR
            JSON_SEARCH(`payment_method`.`allowed_os`,'one', '{$this->userOs}') IS NOT NULL 
        )
        AND (
            `payment_method`.`restricted_products` IS NULL
                OR
            JSON_SEARCH(`payment_method`.`restricted_products`,'one', '{$this->productType}') IS NULL 
        )
        ORDER BY 
            `payment_method_type`.`position`, `payment_method`.`id`;";


        $res = mysqli_query($sqllink, $sql);

        while ($row = mysqli_fetch_assoc($res)) {

            $button = new PaymentButton();
            $button->setName($row['name']);
            $button->setCommission($row['commission']);
            $button->setImageUrl('/' . $this->lang . '/' . $row['image']);
            $button->setPayUrl($row['url']);

            $buttons[] = $button;
        }

        return $buttons;
    }
}

class PaymentButton
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $commission;

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var string
     */
    private $payUrl;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getCommission(): float
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string
     */
    public function getPayUrl(): string
    {
        return $this->payUrl;
    }

    /**
     * @param string $payUrl
     */
    public function setPayUrl(string $payUrl): void
    {
        $this->payUrl = $payUrl;
    }

}


$productType = $_GET['productType'] ?? 'reward';       // book | reward | walletRefill (пополнение внутреннего кошелька)
$amount      = $_GET['amount'] ?? '0.10';        // any float > 0
$lang        = $_GET['lang'] ?? 'en';      // only en | es | uk
$countryCode = $_GET['countryCode'] ?? 'ES';         // any country code in ISO-3166 format
$userOs      = $_GET['userOs'] ?? 'android';    // android | ios | windows

$paymentTypeSelector = new PaymentTypeSelector($productType, $amount, $lang, $countryCode, $userOs);
$paymentButtons      = $paymentTypeSelector->getButtons();

print_r($paymentButtons);
